# Copyright (c) 2020-2021 Intel Corporation.
# SPDX-License-Identifier: BSD-3-Clause

FROM ubuntu:22.04 AS build

RUN mkdir -p /opt/build && mkdir -p /opt/dist
RUN apt-get update && apt-get upgrade -y && \
  DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    ca-certificates curl cmake && \
  rm -rf /var/lib/apt/lists/*

# install cmake
RUN cd /opt/build && \
    curl -LO https://github.com/Kitware/CMake/releases/download/v3.26.2/cmake-3.26.2-linux-x86_64.sh && \
    mkdir -p /opt/dist//usr/local && \
    /bin/bash cmake-3.26.2-linux-x86_64.sh --prefix=/opt/dist//usr/local --skip-license

# cleanup
RUN rm -rf /opt/dist/usr/local/include && \
    rm -rf /opt/dist/usr/local/lib/pkgconfig && \
    find /opt/dist -name "*.a" -exec rm -f {} \; || echo ""
RUN rm -rf /opt/dist/usr/local/share/doc
RUN rm -rf /opt/dist/usr/local/share/man

FROM continuumio/miniconda3 AS python_img
RUN conda create -p /home/gitlab-runner/python -c conda-forge python=3.11 gcc=10 mkl sphinx zlib &&\
    /home/gitlab-runner/python/bin/pip install toml sphinx-rtd-theme breathe sphinx-sitemap myst_parser jupyter nbmake numpy==1.26 scipy pandas pytest seaborn tornado scikit-learn

FROM nvidia/cuda:12.3.1-devel-ubuntu22.04

LABEL author="Thomas Purcell <purcell@fhi-berlin.mpg.de>"

ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Europe/Berlin"


RUN apt-get update &&\
    apt-get install -y build-essential vim libgcc-10-dev gcc-10 g++-10 gfortran-10 git &&\
    apt-get install -y openssh-client openssh-server rsync &&\
    apt-get install -y openmpi-bin openmpi-common openmpi-doc libopenmpi-dev &&\
    apt-get install -y doxygen &&\
    apt-get install -y lcov gcovr &&\
    apt-get install -y pkgconf &&\
    apt-get install -y zlib1g-dev &&\
    apt-get install -y bzip2 libbz2-dev &&\
    apt-get install -y sudo &&\
    apt-get install -y libopenblas-dev &&\
    apt-get install -y libboost-atomic1.74-dev libboost-chrono1.74-dev libboost-container1.74-dev libboost-filesystem1.74-dev libboost-graph1.74-dev libboost-mpi1.74-dev libboost-random1.74-dev libboost-thread1.74-dev libboost-serialization1.74-dev &&\
    apt-get install -y libnlopt-cxx-dev &&\
    apt-get install -y coinor-libcoinutils-dev coinor-libclp-dev &&\
    apt-get install -y libgtest-dev libgmock-dev &&\
    apt-get install -y libfmt-dev &&\
    apt-get install -y python3-libxml2 &&\
    apt-get clean

COPY --from=build /opt/dist /opt/dist

RUN wget https://apt.kitware.com/kitware-archive.sh && chmod +x kitware-archive.sh
RUN ./kitware-archive.sh
RUN apt-get upgrade -y

RUN git config --global --add safe.directory /builds/tpurcell/cpp_sisso
RUN git config --global --add safe.directory /builds/tpurcell/sissopp

RUN useradd -d /home/gitlab-runner -ms /bin/bash gitlab-runner
RUN usermod -aG sudo gitlab-runner
RUN echo "gitlab-runner ALL= NOPASSWD:/bin/chown -R gitlab-runner /builds" >>/etc/sudoers
USER gitlab-runner
WORKDIR /home/gitlab-runner

COPY --from=python_img --chown=gitlab-runner /home/gitlab-runner/python /home/gitlab-runner/python

ENV LANG=C.UTF-8
ENV FI_PROVIDER_PATH='/usr/lib64/libfabric'
ENV PATH='/opt/dist/usr/local/bin/:/home/gitlab-runner/python/bin/:/opt/dist/usr/local/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
ENV PYTHONPATH='/home/gitlab-runner/python/lib/python3.12/site-packages/'
ENV SETVARS_COMPLETED='1'
CMD ["/bin/bash"]
