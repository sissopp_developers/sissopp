.. _py_api_class

Classification
--------------

.. currentmodule:: sissopp.postprocess.classification

.. autofunction:: update_model_svm

.. currentmodule:: sissopp.postprocess.plot.classification

.. autofunction:: plot_classification
