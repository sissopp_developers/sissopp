#! /bin/bash
export DIRNAME=`dirname $0`
if [ -d feature_space ]; then
    rm -r feature_space/  models/;
fi
mpiexec -n $1 $2 $DIRNAME/sisso.json
