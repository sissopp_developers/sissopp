from sissopp import ModelClassifier
from pathlib import Path

import numpy as np

model = ModelClassifier(
    str("models/train_dim_2_model_0.dat")
)
assert model.percent_error < 1e-7
