// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>

#include <cstdint>
#include <descriptor_identifier/solver/ModelSet.hpp>

TEST(ModelSet, model_generation_dim1)
{
    ModelSet<CPUDevice> best_features(1, 10);
    EXPECT_EQ(best_features._models.extent(0), 1);
    EXPECT_EQ(best_features._models.extent(1), 10);

    auto num_models_generated = best_features.generate_new_models(inds_sc_pair(), 10);
    EXPECT_EQ(num_models_generated, 10);
}

TEST(ModelSet, model_generation_dim2)
{
    ModelSet<CPUDevice> best_features(2, 10);
    EXPECT_EQ(best_features._models.extent(0), 2);
    EXPECT_EQ(best_features._models.extent(1), 10);

    auto num_models_generated = best_features.generate_new_models(inds_sc_pair({5}, 13), 10);
    EXPECT_EQ(num_models_generated, 9);
}
