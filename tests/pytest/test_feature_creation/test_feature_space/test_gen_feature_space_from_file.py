# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import (
    FeatureNode,
    FeatureSpace,
    Unit,
    finalize_values_arr,
    SISSORegressor,
)

import pathlib

parent = pathlib.Path(__file__).parent.absolute()


def test_gen_feature_space_from_file():
    task_sizes_train = [90]
    task_sizes_test = [10]

    finalize_values_arr()
    phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            np.random.uniform(1e-5, 100.0, task_sizes_train[0]),
            np.random.uniform(1e-5, 100.0, task_sizes_test[0]),
            Unit(),
        )
        for ff in range(10)
    ]

    prop = np.power(phi_0[0].value + phi_0[1].value, 2.0)

    feat_space = FeatureSpace(
        f"{parent}/phi.txt", phi_0, prop, task_sizes_train, "regression", 1, 1.0, []
    )
    feat_space.sis(prop)
    assert feat_space.phi_selected[0].postfix_expr == "1|0|add|sq"

    # Test that if an empty centeral storage area is passed to FeatureSpace it will reset it
    finalize_values_arr()
    feat_space = FeatureSpace(
        f"{parent}/phi.txt", phi_0, list(prop), task_sizes_train, "regression", 1, 1.0
    )
    feat_space.sis(prop)
    assert feat_space.phi_selected[0].postfix_expr == "1|0|add|sq"


if __name__ == "__main__":
    test_gen_feature_space_from_file()
