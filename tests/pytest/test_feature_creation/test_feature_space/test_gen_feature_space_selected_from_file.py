# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)
from sissopp import phi_selected_from_file
from sissopp.py_interface import read_csv

import pathlib

parent = pathlib.Path(__file__).parent.absolute()


def test_gen_feature_space_from_file():
    inputs = read_csv(str(parent / "data.csv"), "Prop", task_key="Task", max_rung=2)

    phi_sel = phi_selected_from_file(
        str(parent / "selected_features.txt"), inputs.phi_0, []
    )

    assert phi_sel[0].postfix_expr == "3|2|add|3|abs|add"
    assert phi_sel[1].postfix_expr == "1|0|div|0|div"


if __name__ == "__main__":
    test_gen_feature_space_from_file()
