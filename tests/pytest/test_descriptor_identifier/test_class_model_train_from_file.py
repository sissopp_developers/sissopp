# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp.postprocess.load_models import load_model
from pathlib import Path

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_class_model_train_from_file():
    try:
        model = load_model(
            str(parent / "model_files/train_classifier_fail_overlap.dat"),
        )
        raise ValueError("Model created that should fail")
    except RuntimeError:
        pass

    model = load_model(str(parent / "model_files/train_classifier.dat"))

    assert np.all(np.abs(model.fit - model.prop_train) < 1e-7)

    assert np.sum(model.train_error) + 80 < 1e-7

    assert model.task_sizes_train == [80]
    assert model.task_sizes_test == [0]
    assert model.leave_out_inds == []

    assert model.feats[0].postfix_expr == "9|8|sub"
    assert model.feats[1].postfix_expr == "1|0|mult"

    actual_coefs = [
        [1.326205649731981, -1.744239999671528, 0.9075950727790907],
    ]

    assert np.all(
        [
            abs(coef - actual) < 1e-8
            for coef, actual in zip(model.coefs[0], actual_coefs[0])
        ]
    )
    assert (
        model.latex_str
        == "[$\\left(feat_{9} - feat_{8}\\right)$, $\\left(feat_{1} feat_{0}\\right)$]"
    )


if __name__ == "__main__":
    test_class_model_train_from_file()
