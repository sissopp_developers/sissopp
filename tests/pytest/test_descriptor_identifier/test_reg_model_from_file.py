# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp.postprocess.load_models import load_model
from pathlib import Path

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_reg_model_from_file():
    model = load_model(
        str(parent / "model_files/train_regressor.dat"),
        str(parent / "model_files/test_regressor.dat"),
    )

    mat_fxn_fn = "model_regressor.m"
    mat_fxn_fn_real = str(parent / "matlab_functions" / "model_regressor.m")

    model.write_matlab_fxn(mat_fxn_fn)
    with open(mat_fxn_fn_real) as af:
        actual_lines = af.readlines()

    with open(mat_fxn_fn) as tf:
        test_lines = tf.readlines()

    Path(mat_fxn_fn).unlink()
    for tl, al in zip(test_lines, actual_lines):
        assert tl == al

    assert np.all(np.abs(model.fit - model.prop_train) < 1e-7)
    assert np.all(np.abs(model.predict - model.prop_test) < 1e-7)

    assert np.all(np.abs(model.train_error) < 1e-7)
    assert np.all(np.abs(model.test_error) < 1e-7)

    assert model.task_sizes_train == [57, 38]
    assert model.task_sizes_test == [3, 2]
    assert model.leave_out_inds == [0, 1, 2, 60, 61]

    assert model.feats[0].postfix_expr == "1|0|div|0|div"
    assert model.feats[1].postfix_expr == "3|2|add|3|abs|add"

    actual_coefs = [
        [-7.215478485316414, 82.2718000000000, 3.141589999617774],
        [99.14521320736678, -142.365900000000, -5.254860000134174],
    ]

    assert np.all(
        [
            abs(coef - actual) < 1e-8
            for coef, actual in zip(model.coefs[0], actual_coefs[0])
        ]
    )
    assert np.all(
        [
            abs(coef - actual) < 1e-8
            for coef, actual in zip(model.coefs[1], actual_coefs[1])
        ]
    )

    assert model.r2 > 0.999
    assert model.test_r2 > 0.999

    assert model.rmse < 1e-7
    assert model.test_rmse < 1e-7

    assert model.max_ae < 1e-7
    assert model.test_max_ae < 1e-7

    assert model.mae < 1e-7
    assert model.test_mae < 1e-7

    assert model.mape < 1e-7
    assert model.test_mape < 1e-7

    assert model.percentile_25_ae < 1e-7
    assert model.percentile_25_test_ae < 1e-7

    assert model.percentile_50_ae < 1e-7
    assert model.percentile_50_test_ae < 1e-7

    assert model.percentile_75_ae < 1e-7
    assert model.percentile_75_test_ae < 1e-7

    assert model.percentile_95_ae < 1e-7
    assert model.percentile_95_test_ae < 1e-7
    assert (
        model.latex_str
        == "$c_0 + a_0\\left(\\frac{ \\left(\\frac{ B }{ A } \\right) }{ A } \\right) + a_1\\left(\\left(D + C\\right) + \\left(\\left|D\\right|\\right)\\right)$"
    )

    # Check Pickeling
    pickled = pickle.dumps(model)
    model_unpick = pickle.loads(pickled)

    assert np.allclose(model.prop_train, model_unpick.prop_train)
    assert np.allclose(model.prop_test, model_unpick.prop_test)
    assert np.allclose(model.fit, model_unpick.fit)
    assert np.allclose(model.predict, model_unpick.predict)
    assert np.all(model.task_sizes_train == model_unpick.task_sizes_train)
    assert np.all(model.task_sizes_test == model_unpick.task_sizes_test)
    assert np.all(model.leave_out_inds == model_unpick.leave_out_inds)
    assert np.all(model.sample_ids_train == model_unpick.sample_ids_train)
    assert np.all(model.sample_ids_test == model_unpick.sample_ids_test)
    assert np.all(model.task_names == model_unpick.task_names)
    assert np.all(
        [f1.expr == f2.expr for f1, f2 in zip(model.feats, model_unpick.feats)]
    )
    assert np.allclose(
        np.array(model.coefs).flatten(), np.array(model_unpick.coefs).flatten()
    )
    assert model.fix_intercept == model_unpick.fix_intercept
    assert model.prop_label == model_unpick.prop_label
    assert model.prop_unit == model_unpick.prop_unit


if __name__ == "__main__":
    test_reg_model_from_file()
