# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Visualize the distribution of errors from Cross-Validation

Functions:

plot_validation_rmse: Plots the mean validation rmses/standard deviation using jack-knife resampling
plot_cv_errors: Plot the distribution of errors seen during cross-validation
add_violin: Update the axis to add a violin plot
add_boxplot: Update the axis to add a box plot
add_pointplot: Update the axis to add a point plot
"""
from copy import deepcopy

from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
from sissopp.postprocess.plot import plt, config
from sissopp.postprocess.plot.utils import adjust_box_widths, latexify, setup_plot_ax
from sissopp.postprocess.load_models import get_models
from sissopp.postprocess.get_model_errors import get_model_errors

import numpy as np
import toml

import seaborn as sns


def plot_validation_rmse(models, filename=None, fig_settings=None):
    """Plots the mean validation rmses/standard deviation using jack-knife resampling

    Args:
        models (list of Models or str): The list of models to plot
        filename (str): Name of the file to store the plot in
        fig_settings (dict): non-default plot settings

    Returns:
        matplotlib.pyplot.Figure: The resulting plot
    """
    if isinstance(models, str):
        models = get_models(models)

    if isinstance(fig_settings, str):
        fig_settings = toml.load(fig_settings)

    fig_config, fig, ax = setup_plot_ax(fig_settings)

    n_dim = np.max([model_list[0].n_dim for model_list in models])
    mean_val_rmse, var_val_rmse = jackknife_cv_conv_est(models)

    ax.set_xticks(list(range(1, n_dim + 1)))
    ax.set_xlabel("Model Dimension")
    ax.set_ylabel("Validation RMSE (" + str(models[0][0].prop_unit.latex_str) + ")")

    ax.plot(
        np.arange(1, n_dim + 1, dtype=np.int64),
        mean_val_rmse,
        "o",
        color=fig_config["colors"]["box_test_edge"],
    )
    ax.errorbar(
        np.arange(1, n_dim + 1, dtype=np.int64),
        mean_val_rmse,
        yerr=np.sqrt(var_val_rmse),
        capsize=5.0,
        capthick=1.5,
        linewidth=1.5,
        color=fig_config["colors"]["box_test_edge"],
    )
    if filename:
        fig.savefig(filename)
    return fig


def plot_errors_dists(dir_expr, filename=None, fig_settings=None):
    """Plot the distribution of errors seen during cross-validation

    Args:
        dir_expr (str): Regular expression for the directory list
        filename (str): Name of the file to store the plot in
        fig_settings (dict): non-default plot settings
    """
    # Set up the figure
    if not fig_settings:
        fig_settings = {}

    if isinstance(fig_settings, str):
        fig_settings = toml.load(fig_settings)

    fig_config = deepcopy(config)
    if fig_settings:
        fig_config.update(fig_settings)

    fig_config, fig, ax = setup_plot_ax(
        fig_settings, not fig_config["plot_options"]["std_axis"]
    )

    if "axis_limits" in fig_settings and "y_lim" in fig_settings["axis_limits"]:
        ax.set_ylim(fig_settings["axis_limits"]["y_lim"])

    ax.set_xlabel("Model Dimension")
    models = get_models(dir_expr)

    # Generate the Data and plot_settings object
    train_error, validation_error = get_model_errors(dir_expr, ae=True)
    train_mae = np.nanmean(np.nanmean(np.abs(train_error[:, :, :]), axis=0), axis=0)
    validation_mae = np.nanmean(np.nanmean(validation_error[:, :, :], axis=0), axis=0)

    train_50ae = np.nanmedian(np.nanmedian(train_error[:, :, :], axis=0), axis=0)
    validation_50ae = np.nanmedian(
        np.nanmedian(validation_error[:, :, :], axis=0), axis=0
    )

    train_samp_mean = np.nanmean(train_error, axis=1)
    validation_samp_mean = np.nanmean(validation_error, axis=1)

    dim = np.ones(train_samp_mean.shape)
    for nn in range(dim.shape[1]):
        dim[:, nn] = nn + 1

    data = np.vstack(
        (
            np.column_stack(
                (
                    dim.flatten(),
                    train_samp_mean.flatten(),
                    np.zeros(np.prod(dim.shape)),
                )
            ),
            np.column_stack(
                (
                    dim.flatten(),
                    validation_samp_mean.flatten(),
                    np.ones(np.prod(dim.shape)),
                )
            ),
        )
    )
    mean_data = np.vstack(
        (
            np.column_stack(
                (
                    list(range(1, len(train_mae) + 1)),
                    train_mae,
                    np.zeros(len(train_mae)),
                )
            ),
            np.column_stack(
                (
                    list(range(1, len(validation_mae) + 1)),
                    validation_mae,
                    np.ones(len(validation_mae)),
                )
            ),
        )
    )
    median_data = np.vstack(
        (
            np.column_stack(
                (
                    list(range(1, len(train_50ae) + 1)),
                    train_50ae,
                    np.zeros(len(train_50ae)),
                )
            ),
            np.column_stack(
                (
                    list(range(1, len(validation_50ae) + 1)),
                    validation_50ae,
                    np.ones(len(validation_50ae)),
                )
            ),
        )
    )

    plot_settings = {
        "whiskers": fig_config["features"]["whiskers"],
        "mean_markers": fig_config["features"]["mean_marker_type"],
        "median_markers": fig_config["features"]["median_marker_type"],
        "mean_color": fig_config["features"]["mean_marker_color"],
        "median_color": fig_config["features"]["median_marker_color"],
        "showcaps": fig_config["features"]["show_box_caps"],
        "showfliers": fig_config["features"]["show_box_outliers"],
    }

    if fig_config["plot_options"]["type"] == "split":
        # Set yaxis label
        ax.set_ylabel("Absolute Error (" + str(models[0][0].prop_unit.latex_str) + ")")

        # Populate plot_settings
        plot_settings["colors"] = [
            fig_config["colors"]["train"],
            fig_config["colors"]["test"],
        ]

        plot_settings["box_colors"] = [
            fig_config["colors"]["box_train_face"],
            fig_config["colors"]["box_test_face"],
        ]
        plot_settings["box_edge_colors"] = [
            fig_config["colors"]["box_train_edge"],
            fig_config["colors"]["box_test_edge"],
        ]
    elif fig_config["plot_options"]["type"] == "train":
        # Set yaxis label
        ax.set_ylabel(
            "Absolute Training Error (" + latexify(str(models[0][0].prop_unit)) + ")"
        )

        data = data[: data.shape[0] // 2, :]
        median_data = median_data[: median_data.shape[0] // 2, :]
        mean_data = mean_data[: mean_data.shape[0] // 2, :]

        # Populate plot_settings
        plot_settings["colors"] = [fig_config["colors"]["train"]]
        plot_settings["box_colors"] = [fig_config["colors"]["box_train_face"]]
        plot_settings["box_edge_colors"] = [fig_config["colors"]["box_train_edge"]]

    elif fig_config["plot_options"]["type"] == "test":
        # Set yaxis label
        ax.set_ylabel(
            "Absolute Validation Error (" + latexify(str(models[0][0].prop_unit)) + ")"
        )

        data = data[data.shape[0] // 2 :, :]
        median_data = median_data[median_data.shape[0] // 2 :, :]
        mean_data = mean_data[mean_data.shape[0] // 2 :, :]

        # Populate plot_settings
        plot_settings["colors"] = [fig_config["colors"]["test"]]
        plot_settings["box_colors"] = [fig_config["colors"]["box_test_face"]]
        plot_settings["box_edge_colors"] = [fig_config["colors"]["box_test_edge"]]
    else:
        raise ValueError("Invalid plot type")

    # Create the plot

    # Add a box plot
    if fig_config["plot_options"]["box"]:
        plot_settings["boxprops"] = {"zorder": 2, "linewidth": 1.5}
        plot_settings["whiskerprops"] = {"linewidth": 1.5}

        add_boxplot(ax, data, plot_settings)
    elif not fig_config["plot_options"]["violin"]:
        # Add Mean and Median Markers
        # plot_settings["edge_colors"] = plot_settings["box_edge_colors"]
        plot_settings["palette"] = [fig_config["features"]["median_marker_color"]] * 2
        plot_settings["join"] = fig_config["plot_options"]["connect_median"]
        plot_settings["linestyles"] = [nn * "-" for nn in range(1, 3)]

        add_pointplot(ax, median_data, plot_settings)

        if fig_config["features"]["show_means"]:
            plot_settings["linestyles"][0] = ":"
            if len(plot_settings["linestyles"]) == 2:
                plot_settings["linestyles"] = "-."
            plot_settings["markers"] = plot_settings["mean_markers"]
            plot_settings["palette"] = [fig_config["features"]["mean_marker_color"]] * 2
            plot_settings["join"] = fig_config["plot_options"]["connect_mean"]
            add_pointplot(ax, mean_data, plot_settings)

        # plt.setp(ax.lines, zorder=100)
        # plt.setp(ax.collections, zorder=100, label="")

    # Add the violin plot
    if fig_config["plot_options"]["violin"]:
        plot_settings["split"] = fig_config["plot_options"]["type"] == "split"
        add_violin(ax, data, plot_settings)

        # ax.set_xticklabels(labels=[str(nn) for nn in range(1, len(models) + 1)])

        if fig_config["plot_options"]["type"] == "split":
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles[-2:], ["Train", "Test"], frameon=False)
        else:
            ax.get_legend().remove()
    elif fig_config["plot_options"]["type"] == "split":
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles[:2], ["Train", "Test"], frameon=False)
    else:
        ax.get_legend().remove()

    std = np.hstack((models[0][0].prop_train, models[0][0].prop_test)).std()
    if fig_config["plot_options"]["std_axis"]:
        ax2 = ax.twinx()
        ax2.set_ylabel(ax.yaxis.get_label_text().split("(")[0] + " / std")
        ax2.set_ylim([ax.axis()[-2] / std, ax.axis()[-1] / std])

    if fig_config["plot_options"]["baseline"]:
        ax.axhline(
            y=std,
            xmin=0.0,
            xmax=1.0,
            dashes=[4, 3],
            linewidth=2.0,
            color="black",
            zorder=0,
        )

    if filename:
        fig.savefig(filename)

    return fig


def add_violin(ax, data, plot_settings):
    """Update the axis to add a violin plot

    Args:
        ax (axes.Axes): The Axes object to add the violin plot to
        data (np.ndarray): The data to plot
        fig_config (adict): The settings for the figure
    """
    sns.violinplot(
        x=data[:, 0].astype(np.int64),
        y=data[:, 1],
        hue=data[:, 2],
        split=plot_settings["split"],
        inner=None,
        linewidth=1.0,
        palette=plot_settings["colors"],
        cut=0,
        ax=ax,
        scale="area",
        zorder=-10,
    )


def add_boxplot(ax, data, plot_settings):
    """Update the axis to add a box plot

    Args:
        ax (axes.Axes): The Axes object to add the violin plot to
        data (np.ndarray): The data to plot
        fig_config (adict): The settings for the figure
    """
    sns.boxplot(
        x=data[:, 0].astype(np.int64),
        y=data[:, 1],
        hue=data[:, 2],
        palette=plot_settings["box_colors"],
        width=0.5 / (3 - len(plot_settings["colors"])),
        ax=ax,
        whis=plot_settings["whiskers"],
        linewidth=1.0,
        boxprops=plot_settings["boxprops"],
        medianprops={
            "linewidth": 2.0,
            "color": plot_settings["median_color"],
            "zorder": 2,
        },
        whiskerprops=plot_settings["whiskerprops"],
        showmeans=True,
        meanprops={
            "marker": plot_settings["mean_markers"],
            "markerfacecolor": plot_settings["mean_color"],
            "markeredgewidth": 0.0,
            "zorder": 2,
        },
        showcaps=plot_settings["showcaps"],
        showfliers=plot_settings["showfliers"],
        # dodge=0.5,
    )
    adjust_box_widths(ax, 0.5)
    # inc = len(plot_settings["box_edge_colors"])
    # for ii, artist in enumerate(ax.artists):
    #     col = plot_settings["box_edge_colors"][ii % inc]
    #     artist.set_edgecolor(col)
    #     for jj in range(ii * 3, (ii + 1) * 3):
    #         line = ax.lines[jj]
    #         line.set_color(col)
    #         line.set_mfc(col)
    #         line.set_mec(col)


def add_pointplot(ax, data, plot_settings):
    """Update the axis to add a point plot

    Args:
        ax (axes.Axes): The Axes object to add the violin plot to
        data (np.ndarray): The data to plot
        fig_config (adict): The settings for the figure
    """
    sns.pointplot(
        x=data[:, 0].astype(np.int64),
        y=data[:, 1],
        hue=data[:, 2],
        join=plot_settings["join"],
        ci=None,
        markers=plot_settings["markers"],
        palette=plot_settings["palette"],
        linestyles=plot_settings["linestyles"],
        dodge=0.25,
        zorder=4,
        scale=0.75,
    )
