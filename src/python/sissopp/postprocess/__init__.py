# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Modules to facilitate postprocessing of the results of the SISSO++ code

Modules:

check_cv_convergence: Check the convergence of cross-validation results
classification: Retrain the SVM decision boundaries of a classification model
feature_space_analysis: Analyze the prevalence of the primary features in the selected feature space
load_models: Create Model objects from the output files
plot: Create various plots of the SISSO models
"""
