# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Functions to get the FeatureSpace and SISSOSolver objects needed to run the code.

Functions:

get_fs: Generate a FeatureSpace for the calculation
get_fs_solver: Generate a FeatureSpace and SISSOSolver for the calculation
"""
import numpy as np
import pandas as pd
from pathlib import Path

from sissopp import (
    FeatureSpace,
    SISSORegressor,
    SISSOLogRegressor,
    SISSOClassifier,
)


def get_fs_solver(inputs, allow_overwrite=False):
    """Generate a FeatureSpace and SISSOSolver for the calculation from an Inputs Object

    Args:
        inputs (Inputs): The inputs for the calculation
        allow_overwrite (bool): If True then a previous calculation can be overwritten

    Returns:
        tuple: A tuple containing:
            - feat_space (FeatureSpace): The FeatureSpace for the calculation
            - sr (SISSOSolver): The SISSOSolver for the calculation
    """
    if Path("models/train_dim_1_model_0.dat").exists() and not allow_overwrite:
        raise ValueError(
            "Cannot perform calculation as previous run would be overwritten, to still perform this calculation, set allow_overwrite=True"
        )
    feat_space = FeatureSpace(inputs)

    if inputs.calc_type.lower() == "regression":
        solver = SISSORegressor(inputs, feat_space)
    elif inputs.calc_type.lower() == "log_regression":
        solver = SISSOLogRegressor(inputs, feat_space)
    elif inputs.calc_type.lower() == "classification":
        solver = SISSOClassifier(inputs, feat_space)
    else:
        raise ValueError(
            "The calculation type specified in inputs.calc_type is not supported"
        )
    return feat_space, solver
