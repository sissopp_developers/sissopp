// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_bindings_cpp_def/descriptor_identifier/Model.cpp
 *  @brief Implements the python based functionality of the base class of the output Models for SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/Model.hpp"

py::array_t<double> Model::eval_many_py(py::array_t<double> x_in,
                                        std::vector<std::string> tasks) const
{
    std::vector<std::vector<double>> x_in_vec = python_conv_utils::from_2D_array_transpose<double>(
        x_in);
    std::vector<std::string> task_vec = check_tasks(tasks, x_in_vec[0].size());

    return py::cast(eval(x_in_vec, task_vec));
}

py::array_t<double> Model::eval_many_py(std::map<std::string, py::array_t<double>> x_in,
                                        std::vector<std::string> tasks) const
{
    std::map<std::string, std::vector<double>>
        x_in_map = python_conv_utils::convert_map<std::string, double>(x_in);
    std::vector<std::string> task_vec = check_tasks(tasks, x_in_map.begin()->second.size());

    return py::cast(eval(x_in_map, task_vec));
}
