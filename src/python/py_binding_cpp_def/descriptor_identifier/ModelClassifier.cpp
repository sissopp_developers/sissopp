// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_bindings_cpp_def/descriptor_identifier/ModelClassifier.cpp
 *  @brief Implements the python based functionality of the class for the output models from solving a classification problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/ModelClassifier.hpp"

ModelClassifier::ModelClassifier(const ModelClassifier& o,
                                 std::vector<std::vector<double>> new_coefs,
                                 py::array_t<double> prop_train_est,
                                 py::array_t<double> prop_test_est)
    : Model(o),
      _n_class(o._n_class),
      _train_n_convex_overlap(o._train_n_convex_overlap),
      _test_n_convex_overlap(o._test_n_convex_overlap)
{
    _coefs = new_coefs;
    std::vector<int> misclassified(_n_samp_train);
    std::transform(_loss->prop_train().begin(),
                   _loss->prop_train().end(),
                   _loss->prop_train_est().begin(),
                   misclassified.begin(),
                   [](double p1, double p2) { return p1 != p2; });
    _train_n_svm_misclassified = std::accumulate(misclassified.begin(), misclassified.end(), 0);

    std::vector<int> misclassified_test(_n_samp_test);
    std::transform(_loss->prop_test().begin(),
                   _loss->prop_test().end(),
                   _loss->prop_test_est().begin(),
                   misclassified_test.begin(),
                   [](double p1, double p2) { return p1 != p2; });
    _test_n_svm_misclassified = std::accumulate(
        misclassified_test.begin(), misclassified_test.end(), 0);
}
