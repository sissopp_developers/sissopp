// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/solver/SISSOSolver.cpp
 *  @brief Implements the base class for creating solvers using SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/solver/SISSOSolver.hpp"

#include <omp.h>

#include <limits>

#include "SelectBestFeatures.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/EnumerateUniqueCombinations.hpp"
#include "utils/KokkosDefines.hpp"
#include "utils/PropertiesVector.hpp"
#include "utils/assumption.hpp"
#include "utils/split_range.hpp"

SISSOSolver::SISSOSolver(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space)
    : _sample_ids_train(inputs.sample_ids_train()),
      _sample_ids_test(inputs.sample_ids_test()),
      _task_names(inputs.task_names()),
      _task_sizes_train(inputs.task_sizes_train()),
      _task_sizes_test(inputs.task_sizes_test()),
      _leave_out_inds(inputs.leave_out_inds()),
      _prop_unit(inputs.prop_unit()),
      _prop_label(inputs.prop_label()),
      _feat_space(feat_space),
      _mpi_comm(feat_space->mpi_comm()),
      _n_task(inputs.task_sizes_train().size()),
      _n_samp(inputs.prop_train().size()),
      _n_dim(inputs.n_dim()),
      _n_residual(std::min(static_cast<int>(feat_space->n_sis_select()), inputs.n_residual())),
      _n_models_store(std::min(static_cast<int>(feat_space->n_sis_select()), inputs.n_models_store())),
      _fix_intercept(inputs.fix_intercept()),
      _num_cpu_threads(inputs.get_num_cpu_threads()),
      _num_gpu_threads(inputs.get_num_gpu_threads()),
      _use_single_precision(inputs.use_single_precision()),
      _max_batch_size(inputs.get_max_batch_size()),
      _regularization_algorithm(inputs.regularization_algorithm()),
      _verbose(!inputs.quiet())
{
    if (_mpi_comm->rank() == 0)
    {
        if (inputs.n_residual() > feat_space->n_sis_select())
        {
            std::cerr << "n_residuals is greater than n_sis_select. Setting it to " << feat_space->n_sis_select() << std::endl;
        }
        if (inputs.n_models_store() > feat_space->n_sis_select())
        {
            std::cerr << "_n_models_stores is greater than n_sis_select. Setting it to " << feat_space->n_sis_select() << std::endl;
        }
    }
    if (node_value_arrs::TASK_SZ_TEST.size() == 0)
    {
        node_value_arrs::set_task_sz_test(_task_sizes_test);
    }

    _loss = loss_function_util::get_loss_function(inputs.calc_type(),
                                                  inputs.prop_train(),
                                                  inputs.prop_test(),
                                                  _task_sizes_train,
                                                  _task_sizes_test,
                                                  _fix_intercept);
}

SISSOSolver::SISSOSolver(std::vector<std::string> sample_ids_train,
                         std::vector<std::string> sample_ids_test,
                         std::vector<std::string> task_names,
                         std::vector<int> task_sizes_train,
                         std::vector<int> task_sizes_test,
                         std::vector<int> leave_out_inds,
                         Unit prop_unit,
                         std::string prop_label,
                         std::vector<double> prop_train,
                         std::vector<double> prop_test,
                         std::string calc_type,
                         std::shared_ptr<FeatureSpace> feat_space,
                         int n_dim,
                         int n_residual,
                         int n_models_store,
                         bool fix_intercept,
                         int num_cpu_threads,
                         int num_gpu_threads,
                         bool use_single_precision,
                         int max_batch_size,
                         const RegularizationAlgorithm& regularization_algorithm)
    : _sample_ids_train(sample_ids_train),
      _sample_ids_test(sample_ids_test),
      _task_names(task_names),
      _task_sizes_train(task_sizes_train),
      _task_sizes_test(task_sizes_test),
      _leave_out_inds(leave_out_inds),
      _prop_unit(prop_unit),
      _prop_label(prop_label),
      _feat_space(feat_space),
      _mpi_comm(mpi_setup::comm),
      _n_task(task_sizes_train.size()),
      _n_samp(sample_ids_train.size()),
      _n_dim(n_dim),
      _n_residual(n_residual),
      _n_models_store(n_models_store),
      _fix_intercept(fix_intercept),
      _num_cpu_threads(num_cpu_threads),
      _num_gpu_threads(num_gpu_threads),
      _use_single_precision(use_single_precision),
      _max_batch_size(max_batch_size),
      _regularization_algorithm(regularization_algorithm),
      _verbose(true)
{
    assert(static_cast<int>(sample_ids_train.size()) ==
           std::accumulate(task_sizes_train.begin(), task_sizes_train.end(), 0));
    assert(static_cast<int>(sample_ids_test.size()) ==
           std::accumulate(task_sizes_test.begin(), task_sizes_test.end(), 0));
    assert(sample_ids_train.size() == prop_train.size());
    assert(sample_ids_test.size() == prop_test.size());
    assert(sample_ids_test.size() == leave_out_inds.size());
    assert(task_sizes_train.size() == task_sizes_test.size());
    assert(task_sizes_train.size() == task_names.size());

    if (node_value_arrs::TASK_SZ_TEST.size() == 0)
    {
        node_value_arrs::set_task_sz_test(_task_sizes_test);
    }

    _loss = loss_function_util::get_loss_function(calc_type, prop_train, prop_test, _task_sizes_train, _task_sizes_test, _fix_intercept);
}

void SISSOSolver::l0_regularization(const int n_dim)
{
    const int n_get_models = std::max(_n_residual, _n_models_store);

    std::vector<int> inds(n_dim, 0);
    std::vector<inds_sc_pair> min_sc_inds(n_get_models,
                                          inds_sc_pair(std::vector<int>(n_dim, -1),
                                                       std::numeric_limits<double>::infinity()));

    unsigned long long int n_interactions = 1;
    int n_dim_fact = 1;
    for (int rr = 0; rr < n_dim; ++rr)
    {
        inds[rr] = _feat_space->phi_selected().size() - 1 - rr;
        n_interactions *= inds[rr] + 1;
        n_dim_fact *= (rr + 1);
    }
    n_interactions /= n_dim_fact;
    setup_regulairzation();

#pragma omp declare reduction(n_max_inds : std::vector<inds_sc_pair> : omp_out=openmp_red::n_max<inds_sc_pair>(omp_out, omp_in)) initializer(omp_priv = omp_orig)
    if (inds.back() >= 0)
    {
#pragma omp parallel firstprivate(inds) shared(min_sc_inds)
        {
            std::shared_ptr<LossFunction> loss_copy;
#pragma omp critical
            {
                loss_copy = loss_function_util::copy(_loss);
            }
#pragma omp barrier

            int max_error_ind = 0;
            unsigned long long int ii_prev = 0;

#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(n_max_inds : min_sc_inds)
#else
#pragma omp for schedule(dynamic) reduction(n_max_inds : min_sc_inds)
#endif
            for (unsigned long long int ii = _mpi_comm->rank(); ii < n_interactions;
                 ii += static_cast<unsigned long long int>(_mpi_comm->size()))
            {
                util_funcs::iterate(inds, inds.size(), ii - ii_prev);
                ii_prev = ii;
                double score = (*loss_copy)(inds);

                if (score <= min_sc_inds[max_error_ind].score())
                {
                    update_min_inds_scores(inds, score, max_error_ind, min_sc_inds);
                    max_error_ind = std::max_element(min_sc_inds.begin(), min_sc_inds.end()) -
                                    min_sc_inds.begin();
                }
            }
        }
    }
    std::vector<inds_sc_pair> all_min_sc_inds(_mpi_comm->size() * n_get_models);

    mpi::all_gather(*_mpi_comm, min_sc_inds.data(), n_get_models, all_min_sc_inds);

    inds = util_funcs::argsort<inds_sc_pair>(all_min_sc_inds);
    std::vector<std::vector<int>> indexes(n_get_models, std::vector<int>(n_dim));
    for (int rr = 0; rr < n_get_models; ++rr)
    {
        node_value_arrs::clear_temp_test_reg();
        indexes[rr] = all_min_sc_inds[inds[rr]].obj();
        if (indexes[rr].size() == 0)
        {
            throw std::logic_error("A selected model contains no features.");
        }
    }
    add_models(indexes);
}

/**
 * @tparam Precision double or float
 * @param n_dim dimension of the tuple space to be evaluated
 * @return best models
 */
template <typename Precision>
std::vector<inds_sc_pair> SISSOSolver::get_best_higher_dimensional_models(const int n_dim)
{
    int num_threads = _num_cpu_threads + _num_gpu_threads;

    const int n_get_models = std::max(_n_residual, _n_models_store);

    setup_regulairzation();

    DescriptorMatrix<typename CPUDevice::memory_space, Precision> h_descriptorMatrix;
    PropertiesVector<typename CPUDevice::memory_space, Precision> h_properties(_loss->prop_train());
    DescriptorMatrix<typename GPUDevice::memory_space, Precision> d_descriptorMatrix;
    PropertiesVector<typename GPUDevice::memory_space, Precision> d_properties(_loss->prop_train());

    EnumerateUniqueCombinations feature_combinations(_feat_space->phi_selected().size() - 1, n_dim);
    if (_verbose && (_mpi_comm->rank() == 0))
    {
        std::cout << fmt::format("solving {} combinations of {}-tuples in chunks of {}", feature_combinations.get_num_total_combinations(), n_dim, _max_batch_size) << std::endl;
    }
    feature_combinations += _mpi_comm->rank();

    std::vector<std::shared_ptr<ISelectBestFeatures>> solvers(num_threads, nullptr);

    //    log(fmt::format("prepared {:d} threads", omp_get_max_threads()));
    if (_verbose && (_mpi_comm->rank() == 0))
    {
        std::cout << fmt::format("preparing {:d} gpu threads and {:d} cpu threads per rank",
                                 _num_gpu_threads,
                                 _num_cpu_threads)
                  << std::endl;
    }

#pragma omp parallel firstprivate(n_get_models) shared(std::cout,            \
                                                       solvers,              \
                                                       feature_combinations, \
                                                       h_descriptorMatrix,   \
                                                       h_properties,         \
                                                       d_descriptorMatrix,   \
                                                       d_properties)         \
    num_threads(num_threads)
    {
        if (omp_get_thread_num() < _num_gpu_threads)
        {
            solvers[omp_get_thread_num()] = std::make_shared<SelectBestFeaturesGPU<Precision>>(
                n_get_models,
                _max_batch_size,
                _max_batch_size,
                d_descriptorMatrix.getMatrix(),
                d_properties.getVector(),
                _loss->task_sizes_train(),
                _loss->fix_intercept(),
                _loss->n_feat());
        }
        else
        {
            solvers[omp_get_thread_num()] = std::make_shared<SelectBestFeaturesCPU<Precision>>(
                n_get_models,
                1,
                _max_batch_size,
                h_descriptorMatrix.getMatrix(),
                h_properties.getVector(),
                _loss->task_sizes_train(),
                _loss->fix_intercept(),
                _loss->n_feat());
        }
#pragma omp single nowait
        {
            int64_t package_idx = 0;

            while (!feature_combinations.is_finished())
            {
//                log(fmt::format(
//                    "{:4d} creates work package {:4d}", omp_get_thread_num(), package_idx));
#pragma omp task shared(std::cout, solvers) \
    firstprivate(feature_combinations, package_idx) default(none)
                {
                    //                    log(fmt::format(
                    //                        "{:4d} solves work package {:4d}", omp_get_thread_num(), package_idx));
                    solvers[omp_get_thread_num()]->evaluate(
                        feature_combinations, _max_batch_size, _mpi_comm->size(), package_idx);
                }
                feature_combinations += _mpi_comm->size() * _max_batch_size;
                ++package_idx;
            }
        }
    }

    for (auto thread_idx = 1; thread_idx < solvers.size(); ++thread_idx)
    {
        solvers[0]->best_feature_combinations.merge(solvers[thread_idx]->best_feature_combinations);
    }

    return solvers[0]->best_feature_combinations.data();
}

template <typename Precision>
std::vector<inds_sc_pair> SISSOSolver::get_best_higher_dimensional_models(
    int64_t num_models, const std::vector<inds_sc_pair>& old_models)
{
    auto mpi_local_range = split_range(
        {0, old_models.size()}, _mpi_comm->rank(), _mpi_comm->size());
    const auto begin = old_models.begin() + mpi_local_range.first;
    const auto end = old_models.begin() + mpi_local_range.second;

    if ((end - begin) == 0) return std::vector<inds_sc_pair>();

    const int64_t num_dimensions = int64_t(begin->obj().size()) + 1;

    int num_threads = _num_cpu_threads + _num_gpu_threads;

    DescriptorMatrix<typename CPUDevice::memory_space, Precision> h_descriptorMatrix;
    PropertiesVector<typename CPUDevice::memory_space, Precision> h_properties(_loss->prop_train());
    DescriptorMatrix<typename GPUDevice::memory_space, Precision> d_descriptorMatrix;
    PropertiesVector<typename GPUDevice::memory_space, Precision> d_properties(_loss->prop_train());

    int64_t num_features = _feat_space->phi_selected().size();
    if (_verbose)
    {
        std::cout
            << fmt::format(
                   "[{}] with {} features solving {} combinations of {}-tuples, boiling down to {} in "
                   "the end",
                   _mpi_comm->rank(),
                   num_features,
                   (end - begin) * num_features,
                   num_dimensions,
                   num_models)
            << std::endl;
    }

    std::vector<std::shared_ptr<ISelectBestFeatures>> solvers(num_threads, nullptr);

    //    log(fmt::format("prepared {:d} threads", omp_get_max_threads()));

#pragma omp parallel num_threads(num_threads) firstprivate(num_models) shared(std::cout,          \
                                                                              solvers,            \
                                                                              h_descriptorMatrix, \
                                                                              h_properties,       \
                                                                              d_descriptorMatrix, \
                                                                              d_properties,       \
                                                                              num_features)
    {
        if (omp_get_thread_num() < _num_gpu_threads)
        {
            //            log(fmt::format("{:4d} creating GPU solver", omp_get_thread_num()));
            solvers[omp_get_thread_num()] = std::make_shared<SelectBestFeaturesGPU<Precision>>(
                num_models,
                _max_batch_size,
                _max_batch_size,
                d_descriptorMatrix.getMatrix(),
                d_properties.getVector(),
                _loss->task_sizes_train(),
                _loss->fix_intercept(),
                _loss->n_feat());
        }
        else
        {
            //            log(fmt::format("{:4d} creating CPU solver", omp_get_thread_num()));
            solvers[omp_get_thread_num()] = std::make_shared<SelectBestFeaturesCPU<Precision>>(
                num_models,
                1,
                num_models,
                h_descriptorMatrix.getMatrix(),
                h_properties.getVector(),
                _loss->task_sizes_train(),
                _loss->fix_intercept(),
                _loss->n_feat());
        }
#pragma omp single
        {
            int64_t package_idx = 0;

            for (auto modelIt = begin; modelIt != end; ++modelIt)
            {
//                log(fmt::format(
//                    "{:4d} creates work package {:4d}", omp_get_thread_num(), package_idx));
#pragma omp task shared(std::cout, solvers) \
    firstprivate(modelIt, package_idx, num_features) default(none)
                {
                    //                    log(fmt::format(
                    //                        "{:4d} solves work package {:4d}", omp_get_thread_num(), package_idx));
                    solvers[omp_get_thread_num()]->evaluate(*modelIt, num_features, package_idx);
                }
                ++package_idx;
            }
        }
    }

    for (auto thread_idx = 1; thread_idx < solvers.size(); ++thread_idx)
    {
        solvers[0]->best_feature_combinations.merge(solvers[thread_idx]->best_feature_combinations);
    }

    return solvers[0]->best_feature_combinations.data();
}

template <typename Precision>
std::vector<inds_sc_pair> SISSOSolver::l0_regularization_gpu(
    const int /*n_dim*/, const std::vector<inds_sc_pair>& old_models)
{
    const int n_get_models = _n_residual;

    setup_regulairzation();

    std::vector<inds_sc_pair> best_local_models;
    if (_regularization_algorithm == RegularizationAlgorithm::FULL_EXPLORATION)
    {
        best_local_models = get_best_higher_dimensional_models<Precision>(old_models.at(0).obj().size() + 1);
    }
    if (_regularization_algorithm == RegularizationAlgorithm::GREEDY_EXPLORATION)
    {
        best_local_models = get_best_higher_dimensional_models<Precision>(n_get_models, old_models);
    }

    auto all_min_sc_inds = all_reduce_models(n_get_models, best_local_models);

    add_models(all_min_sc_inds);

    return all_min_sc_inds;
}
template std::vector<inds_sc_pair> SISSOSolver::l0_regularization_gpu<float>(
    const int n_dim, const std::vector<inds_sc_pair>& old_models);
template std::vector<inds_sc_pair> SISSOSolver::l0_regularization_gpu<double>(
    const int n_dim, const std::vector<inds_sc_pair>& old_models);

std::vector<inds_sc_pair> SISSOSolver::all_reduce_models(
    const int n_get_models, std::vector<inds_sc_pair>& best_local_models)
{
    best_local_models.resize(n_get_models,
                             inds_sc_pair(std::vector<int>{0}, std::numeric_limits<double>::max()));
    CHECK_EQUAL(best_local_models.size(),
                n_get_models,
                "all vectors need to have the same size, otherwise all_gather will crash");

    std::vector<inds_sc_pair> all_min_sc_inds(_mpi_comm->size() * n_get_models);
    mpi::all_gather(*_mpi_comm, best_local_models.data(), n_get_models, all_min_sc_inds);

    std::partial_sort(
        all_min_sc_inds.begin(), all_min_sc_inds.begin() + n_get_models, all_min_sc_inds.end());
    all_min_sc_inds.erase(all_min_sc_inds.begin() + n_get_models, all_min_sc_inds.end());
    all_min_sc_inds.shrink_to_fit();
    CHECK_EQUAL(all_min_sc_inds.size(), n_get_models, "");

    return all_min_sc_inds;
}

void SISSOSolver::add_models(const std::vector<inds_sc_pair>& models)
{
    const int n_save_models = std::max(_n_residual, _n_models_store);
    CHECK_LESS_EQUAL(n_save_models, models.size(), "");
    std::vector<std::vector<int>> indexes(n_save_models);
    for (int rr = 0; rr < n_save_models; ++rr)
    {
        node_value_arrs::clear_temp_test_reg();
        indexes[rr] = models[rr].obj();
        if (indexes[rr].size() == 0)
        {
            throw std::logic_error("A selected model contains no features.");
        }
    }
    add_models(indexes);
}

void SISSOSolver::fit()
{
    std::vector<inds_sc_pair> models(1);

    int dd = cur_dim();
    while (continue_calc(dd))
    {
        _loss->set_nfeat(dd);

        double start = omp_get_wtime();
        std::shared_ptr<Projector> projector;

        std::vector<std::shared_ptr<Model>> cur_model_ptrs = get_cur_modles();
        if (cur_model_ptrs.size() > 0)
        {
            projector = projector_util::get_projector(_feat_space->project_type(), cur_model_ptrs);
        }
        else
        {
            projector = projector_util::get_projector(_feat_space->project_type(),
                                                      _loss->prop_train(),
                                                      _loss->task_sizes_train());
        }
        _feat_space->sis(projector);

        _mpi_comm->barrier();
        double duration = omp_get_wtime() - start;
        if (_verbose && (_mpi_comm->rank() == 0))
        {
            std::cout << "Time for SIS: " << duration << " s" << std::endl;
        }

        start = omp_get_wtime();
        if (_loss->type() == LOSS_TYPE::PEARSON_RMSE_GPU)
        {
            if (_use_single_precision)
            {
                models = l0_regularization_gpu<float>(dd, models);
            }
            else
            {
                models = l0_regularization_gpu<double>(dd, models);
            }
        }
        else
        {
            l0_regularization(dd);
        }

        _mpi_comm->barrier();
        duration = omp_get_wtime() - start;

        if (_verbose && (_mpi_comm->rank() == 0))
        {
            std::cout << "Time for l0-norm: " << duration << " s" << std::endl;
        }
        output_models();
        ++dd;
    }
}
