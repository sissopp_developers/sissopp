// Copyright 2023 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/Projector.hpp
 *  @brief Defines a base class used to calculate the projection score and l0-regularization objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PROJECTOR
#define PROJECTOR

#include "descriptor_identifier/model/Model.hpp"

// DocString: cls_projector
/**
 * @brief The projection function used during SIS
 *
 */
class Projector
{
protected:
    // clang-format off
    const std::vector<double> _prop; //!< The property to project against
    std::vector<double> _scores; //!< The projection scores for each property vector
    const std::vector<int> _task_sizes; //!< Number of training samples per task

    const int _n_samp; //!< Number of samples in the training set
    const int _n_task; //!< Number of tasks
    const int _n_project_prop; //!< Number of properties to project over
    // clang-format on
public:

    /**
     * @brief Constructor
     *
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param task_sizes_train Number of training samples per task
     */
    Projector(std::vector<double> prop, std::vector<int> task_sizes_train);

    /**
     * @brief Copy constructor
     *
     * @param o Pointer to the projector to be copied
     */
    Projector(std::shared_ptr<Projector> o);

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    virtual double project(const node_ptr& feat) = 0;

    // DocString: projector_type
    /**
     * @brief The type of the projector
     */
    virtual PROJECT_TYPE type() const = 0;

    // DocString: projector_prop
    /**
     * @brief The property vector used for projection
     */
    inline std::vector<double> prop() const { return _prop;}

    // DocString: projector_scores
    /**
     * @brief The standardized property vector used for projection
     */
    inline std::vector<double> scores() const { return _scores;}

    // DocString: projector_task_sizes_train
    /**
     * @brief Number of training samples per task
     */
    inline std::vector<int> task_sizes() const { return _task_sizes;}

    // DocString: projector_n_samp
    /**
     * @brief Number of samples in the training set
     */
    inline int n_samp() const { return _n_samp;}

    // DocString: projector_n_task
    /**
     * @brief Number of tasks
     */
    inline int n_task() const { return _n_task;}

    // DocString: projector_n_project_prop
    /**
     * @brief Number of properties to project over
     */
    inline int n_project_prop() const { return _n_project_prop;}
};

#ifdef PY_BINDINGS
class PyPrjector : public Projector
{
    using Projector::Projector;

    double project(const node_ptr& feat) override
    {
        PYBIND11_OVERRIDE_PURE(double, Projector, project, feat);
    }

    PROJECT_TYPE type() const override { PYBIND11_OVERRIDE_PURE(PROJECT_TYPE, Projector, type, ); }
};
#endif

#endif
