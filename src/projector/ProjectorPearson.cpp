#include "projector/ProjectorPearson.hpp"

ProjectorPearson::ProjectorPearson(std::vector<double> prop, std::vector<int> task_sizes_train) :
    Projector(prop, task_sizes_train),
    _prop_std(_n_project_prop * _n_samp),
    _c(_n_project_prop * _n_task)
{
    standardize_prop(_prop.data(), _prop_std.data());
}

ProjectorPearson::ProjectorPearson(std::shared_ptr<Projector> o) :
    Projector(o),
    _prop_std(_n_project_prop * _n_samp),
    _c(_n_project_prop * _n_task)
{
    standardize_prop(_prop.data(), _prop_std.data());
}

double ProjectorPearson::project(const node_ptr& feat)
{
    std::fill_n(_scores.begin(), _n_project_prop, 0.0);

    return calc_max_pearson(feat->value_ptr(-1, true));
}

void ProjectorPearson::standardize_prop(const double* prop, double* std_prop)
{
    for (int pp = 0; pp < _n_project_prop; ++pp)
    {
        int start = 0;
        for (int tt = 0; tt < _n_task; ++tt)
        {
            util_funcs::standardize(prop + start + _n_samp * pp,
                                    _task_sizes[tt],
                                    std_prop + start * _n_project_prop + pp * _task_sizes[tt]);
            start += _task_sizes[tt];
        }
    }
}

double ProjectorPearson::calc_max_pearson(double* feat_val_ptr)
{
    int start = 0;
    for (size_t tt = 0; tt < _n_task; ++tt)
    {
        double feat_std = util_funcs::stand_dev(feat_val_ptr + start, _task_sizes[tt]);
        dgemm_('T',
               'N',
               _n_project_prop,
               1,
               _task_sizes[tt],
               1.0 / feat_std / static_cast<double>(_task_sizes[tt]),
               &_prop_std[start * _n_project_prop],
               _task_sizes[tt],
               feat_val_ptr + start,
               _task_sizes[tt],
               0.0,
               &_c[tt * _n_project_prop],
               _n_project_prop);

        std::transform(_scores.begin(),
                       _scores.end(),
                       &_c[tt * _n_project_prop],
                       _scores.begin(),
                       [](double score, double cc) { return score + cc * cc; });

        start += _task_sizes[tt];
    }

    return *std::max_element(_scores.begin(), _scores.end()) / static_cast<double>(-1 * _n_task);
}
