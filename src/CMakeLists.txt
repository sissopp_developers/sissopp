file(GLOB_RECURSE SISSOPP_SOURCES *.cpp)
file(GLOB_RECURSE NOT_SISSOPP_SOURCES python/*.cpp)
file(GLOB_RECURSE PYBIND_SOURCES external/pybind11/*.cpp)

list(REMOVE_ITEM SISSOPP_SOURCES ${PYBIND_SOURCES})
list(REMOVE_ITEM SISSOPP_SOURCES ${NOT_SISSOPP_SOURCES})
list(REMOVE_ITEM SISSOPP_SOURCES ${CMAKE_CURRENT_LIST_DIR}/main.cpp)

if(NOT BUILD_PARAMS)
    file(GLOB_RECURSE NOT_SISSOPP_SOURCES */parameterized*.cpp)
    list(REMOVE_ITEM SISSOPP_SOURCES ${NOT_SISSOPP_SOURCES})

    file(GLOB_RECURSE NOT_SISSOPP_SOURCES nl_opt/*)
    list(REMOVE_ITEM SISSOPP_SOURCES ${NOT_SISSOPP_SOURCES})

    list(REMOVE_ITEM SISSOPP_SOURCES ${CMAKE_CURRENT_LIST_DIR}/feature_creation/node/operator_nodes/allowed_parameter_ops.cpp)
endif()

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/sisso++_config.hpp.in ${CMAKE_CURRENT_BINARY_DIR}/sisso++_config.hpp)

if(NOT BUILD_SHARED_LIBS)
    add_library(libsisso STATIC ${SISSOPP_SOURCES})
    set_target_properties(libsisso PROPERTIES POSITION_INDEPENDENT_CODE TRUE)
else()
    add_library(libsisso SHARED ${SISSOPP_SOURCES})
endif()

set_target_properties(libsisso
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
    PREFIX ""
)
if(BUILD_PARAMS)
    target_compile_definitions(libsisso PUBLIC "PARAMETERIZE")
endif()

if(BUILD_CALIPER)
    target_compile_definitions(libsisso PUBLIC "PROFILE_W_CALIPER")
endif()

if (OPENMP_FOUND)
    if((OpenMP_CXX_SPEC_DATE GREATER_EQUAL "201511"))
        target_compile_definitions(libsisso PUBLIC "OMP45")
    endif()
endif(OPENMP_FOUND)

if (Kokkos_ENABLE_CUDA)
    find_package(CUDAToolkit REQUIRED)
    target_link_libraries(libsisso PUBLIC CUDA::cudart CUDA::cublas)

    set(Kokkos_ENABLE_CUDA_CONSTEXPR OFF CACHE BOOL "" FORCE)
    set(Kokkos_ENABLE_CUDA_LAMBDA ON CACHE BOOL "" FORCE)
    set(Kokkos_ENABLE_CUDA_UVM ON CACHE BOOL "" FORCE)
endif()

if (Kokkos_ENABLE_HIP)
    find_package(hip REQUIRED)
    target_link_libraries(libsisso PUBLIC hip::device)

    find_package(rocsolver REQUIRED)
    target_link_libraries(libsisso PUBLIC roc::rocsolver)
endif()

set(Kokkos_ENABLE_SERIAL ON CACHE BOOL "" FORCE)

include(FetchContent)
FetchContent_Declare(
    Kokkos
    GIT_REPOSITORY https://github.com/kokkos/kokkos.git
    GIT_TAG 3.7.00
)
FetchContent_MakeAvailable(Kokkos)

target_link_libraries(libsisso PUBLIC Kokkos::kokkos)
target_link_libraries(libsisso PUBLIC -Wl,--rpath=${LAPACK_DIR} ${LAPACK_LIBRARIES})
target_link_libraries(libsisso PUBLIC ${MPI_LIBRARIES})
target_link_libraries(libsisso PUBLIC -Wl,--rpath=${Boost_LIB_DIR} ${Boost_LIBRARIES})
target_link_libraries(libsisso PUBLIC ${OPENMP_LIBRARIES} )
install(TARGETS libsisso DESTINATION ${CMAKE_INSTALL_PREFIX}/lib/)

if(BUILD_EXE)
    add_executable(sisso++ ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
    set_target_properties(sisso++
        PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib"
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin"
    )
    add_dependencies(sisso++ libsisso)
    target_link_libraries(sisso++ PRIVATE libsisso)
    # target_link_libraries(sisso++ PUBLIC libsisso ${LAPACK_LIBRARIES} ${MPI_LIBRARIES} -Wl,--rpath=${Boost_LIB_DIR} -Wl,--rpath=${LAPACK_DIR} ${OPENMP_LIBRARIES} )
    # target_link_libraries(sisso++ PUBLIC Kokkos::kokkos)

    install(TARGETS sisso++ DESTINATION ${CMAKE_INSTALL_PREFIX}/bin/)

    # Add test for linking to the main executable
    add_test(
        NAME Regression
        COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} 1 ${MPIEXEC_PREFLAGS} "${CMAKE_BINARY_DIR}/bin/sisso++" "${CMAKE_SOURCE_DIR}/tests/exec_test/default/sisso.json" ${MPIEXEC_POSTFLAGS}
    )
endif()

if(BUILD_PYTHON)
    # Add the ability to transfer C++ docstrings into python and add pybind utilities
    include(${CMAKE_SOURCE_DIR}/cmake/TransferDocStrings.cmake)
    add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/external/pybind11)

    list(APPEND CMAKE_INSTALL_RPATH ${PYTHON_PREFIX}/lib/;${PYTHON_INSTDIR}/sissopp/)

    # Transfer the doc strings
    transfer_doc_string(${CMAKE_CURRENT_LIST_DIR}/python/py_binding_cpp_def/bindings_docstring_keyed.cpp ${CMAKE_CURRENT_LIST_DIR}/python/py_binding_cpp_def/bindings.cpp)
    transfer_doc_string(${CMAKE_CURRENT_LIST_DIR}/python/py_binding_cpp_def/bindings_docstring_keyed.hpp ${CMAKE_CURRENT_LIST_DIR}/python/py_binding_cpp_def/bindings.hpp)

    file(GLOB_RECURSE SISSOLIB_SOURCES ${CMAKE_CURRENT_LIST_DIR}/python/*cpp)
    list(REMOVE_ITEM SISSOLIB_SOURCES ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
    list(REMOVE_ITEM SISSOLIB_SOURCES ${CMAKE_CURRENT_LIST_DIR}/python/py_binding_cpp_def/bindings_docstring_keyed.cpp)
    if(NOT BUILD_PARAMS)
        file(GLOB_RECURSE NOT_SISSOLIB_SOURCES */parameterize*.cpp)
        list(REMOVE_ITEM SISSOLIB_SOURCES ${NOT_SISSOLIB_SOURCES})
        list(REMOVE_ITEM SISSOLIB_SOURCES ${CMAKE_CURRENT_LIST_DIR}/feature_creation/node/operator_nodes/allowed_parameter_ops.cpp)
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Coverage")
        pybind11_add_module(_sissopp ${SISSOLIB_SOURCES} NO_EXTRAS)
    else()
        pybind11_add_module(_sissopp ${SISSOLIB_SOURCES})
    endif()

    add_dependencies(_sissopp libsisso)

    set_target_properties(_sissopp
        PROPERTIES
        COMPILE_DEFINITIONS "PY_BINDINGS"
    )
    # target_link_libraries(_sissopp PRIVATE libsisso -Wl,--rpath=${PYTHON_PREFIX}/lib/ ${MPI_LIBRARIES} ${LAPACK_LIBRARIES} ${OPENMP_LIBRARIES} libz.so)
    target_link_libraries(_sissopp PRIVATE libsisso libz.so)

    message(STATUS "${PYTHON_INSTDIR}/sissopp/")
    install(TARGETS libsisso DESTINATION "${PYTHON_INSTDIR}/sissopp/")
    if(NOT SETUP_PY)
        install(TARGETS _sissopp DESTINATION "${PYTHON_INSTDIR}/")
        install(
            DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/python/sissopp/ DESTINATION ${PYTHON_INSTDIR}/sissopp
            FILES_MATCHING PATTERN "*.py"
            PATTERN "CMakeFiles" EXCLUDE
        )
        install(
            DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/python/sissopp/ DESTINATION ${PYTHON_INSTDIR}/sissopp
            FILES_MATCHING PATTERN "*.toml"
            PATTERN "CMakeFiles" EXCLUDE
        )
        install(
            DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/python/sissopp/ DESTINATION ${PYTHON_INSTDIR}/sissopp
            FILES_MATCHING PATTERN "*.mplstyle"
            PATTERN "CMakeFiles" EXCLUDE
        )
    endif()

    # Add tests
    if(BUILD_PARAMS)
        add_test(
            NAME Python_Bindings
            COMMAND pytest "${CMAKE_SOURCE_DIR}/tests/pytest/"
        )
    else(BUILD_PARAMS)
        add_test(
            NAME Python_Bindings
            COMMAND pytest --ignore=${CMAKE_SOURCE_DIR}/tests/pytest/test_param.py --ignore=${CMAKE_SOURCE_DIR}/tests/pytest/test_feature_creation/test_parameterize/ --ignore=${CMAKE_SOURCE_DIR}/tests/pytest/test_model_eval/test_param_model_node/ --ignore=${CMAKE_SOURCE_DIR}/tests/pytest/test_model_eval/test_models/test_reg_model_param.py --ignore=${CMAKE_SOURCE_DIR}/tests/pytest/test_nl_opt/ ${CMAKE_SOURCE_DIR}/tests/pytest/
        )
    endif()
endif()

# Create the coverage target. Run coverage tests with 'make coverage'
string(COMPARE EQUAL ${CMAKE_CXX_COMPILER_ID} "GNU" GNU_COMP)
string(COMPARE EQUAL ${CMAKE_CXX_COMPILER_ID} "Intel" INTEL_COMP)
