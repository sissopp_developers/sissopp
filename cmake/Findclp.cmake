find_path(COIN_CLP_INCLUDE_DIRS
    NAMES "coin/ClpSimplex.hpp"
)

if(${BUILD_SHARED_LIBS})
    set(COIN_CLP_LIB_NAME "libClp.so")
else()
    set(COIN_CLP_LIB_NAME "libClp.la")
endif()

find_library(COIN_CLP_LIBRARY
    ${COIN_CLP_LIB_NAME}
)

get_filename_component(COIN_CLP_LIBRARY_DIRS ${COIN_CLP_LIBRARY} DIRECTORY)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(clp DEFAULT_MSG COIN_CLP_LIBRARY COIN_CLP_INCLUDE_DIRS)

if(clp_FOUND)
    message(STATUS "Found Coin-Clp in ${COIN_CLP_INCLUDE_DIRS}")
    set(COIN_CLP_INCLUDE_DIRS ${COIN_CLP_INCLUDE_DIRS})
    set(COIN_CLP_LIBRARIES ${COIN_CLP_LIBRARY})
else (clp_FOUND)
    message(STATUS "Cannot find Coin-Clp, will try building it from github.")
endif(clp_FOUND)

mark_as_advanced(COIN_CLP_LIBRARIES COIN_CLP_LIBRARY_DIRS COIN_CLP_INCLUDE_DIRS)
