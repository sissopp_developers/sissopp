#! /usr/bin/bash
if($7)
then ../src/external_Clp/configure -C CXX=$3 --prefix=$1 --with-lapack-lib="$2" --with-blas-lib="$2" --libdir=$4 --with-coinutils-lib="$5" --with-coinutils-incdir="$6" ADD_CXXFLAGS="-std=c++14 --shared -fPIC" --enable-shared --disable-static;
else ../src/external_Clp/configure -C CXX=$3 --prefix=$1 --with-lapack-lib="$2" --with-blas-lib="$2" --libdir=$4 --with-coinutils-lib="$5" --with-coinutils-incdir="$6" ADD_CXXFLAGS="-std=c++14 -fPIC" --disable-shared --enable-static;
fi
