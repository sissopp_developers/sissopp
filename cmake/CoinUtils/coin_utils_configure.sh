#! /usr/bin/bash
if($5)
then ../src/external_CoinUtils/configure -C CXX=$3 --prefix=$1 --with-lapack-lflags="$2" --libdir=$4 ADD_CXXFLAGS="-std=c++14 --shared -fPIC" --enable-shared --disable-static;
else ../src/external_CoinUtils/configure -C CXX=$3 --prefix=$1 --with-lapack-lflags="$2" --libdir=$4 ADD_CXXFLAGS="-std=c++14 -fPIC" --disable-shared --enable-static;
fi
